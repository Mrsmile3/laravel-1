<!DOCTYPE html>
<html>
<head><title>Form Sign Up</title></head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome">
        
        <label for="first_name">First name:</label><br><br>
            <input type="text" name="first_name" required><br><br>
        
        <label for="last_name">Last name:</label><br><br>
            <input type="text" name="last_name" required><br><br>
        
        <label for="gender">Gender:</label><br><br>
            <input type="radio" id="Male" name="gender" value="Male" required>
        
            <label for="Male">Male</label><br>
                <input type="radio" id="Female" name="gender" value="Female" required>
            <label for="Female">Female</label><br>
                <input type="radio" id="other" name="gender" value="Other" required>
            <label for="Other">Other</label><br><br>
        
    <label for="nationally">Nationality</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Albania">Albania</option>
            <option value="Bulgaria">Bulgaria</option>
            <option value="Canada">Canada</option>
            <option value="Denmark">Denmark</option>
            <option value="Ethiopia">Ethiopia</option>
            <option value="France">France</option>
            <option value="Germany">Germany</option>
            <option value="Hongkong">Hongkong</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Japan">Japan</option>
            <option value="Kuwait">Kuwait</option>
            <option value="Lebanon">Lebanon</option>
            <option value="Monaco">Monaco</option>
            <option value="Nigeria">Nigeria</option>
        </select><br><br>
        
    <label for="language_spoken">Language Spoken:</label><br><br>
        <input type="checkbox" id="language_1" value="Bahasa Indonesia" name="language_1">
            <label for="languange_1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="language_2" name="language_2" value="English">
            <label for="language_2">English</label><br>
        <input type="checkbox" id="language_3" name="language_3" value="Other">
            <label for="language_3">Arabic</label><br>
        <input type="checkbox" id="language_3" name="language_3" value="Other">
            <label for="language_3">Japanese</label><br><br>
        
    <label for="bio">Bio:</label><br><br>
        <textarea name="bio"></textarea><br>
        
    <input type="submit" name="submit" value="Sign Up">
</form>
</body>
</html>
<!-- Created by Muhammad Habibi Ramadhan a.k.a Zaegelf -->